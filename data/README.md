#####                   README
The data use in discovery, evaluating and validation of the 3PR model is accessible in Plos Medicine.
Simply download the data (signal, metadata and annotation) files into this directory prior to evoking
the proteinPairRatio function call.

#####                   NB

- To reproduce the ROC AUCs, you will need the metadate for stratification of participant samples
- It is assumed that the input data is a dataframe with row names = proteins and column names = samples

    
#####                  Import supplementary data in R & generate the 3PR scores for the ACS data as follows:
- library(openxlsx)
- somaData <- read.xlsx("data/Penn-NicholsonProteinAppendix.xlsx", sheet = 4, startRow = 4)
- rownames(somaData) <- somaData$ProteinID
- somaData <- somaData[,-c(1)]
- somaData[,1:dim(somaData)[2]]<- apply(somaData[,1:dim(somaData)[2]], 2, function (x) as.numeric(as.character(x)))
