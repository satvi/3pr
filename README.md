[South African Tuberculosis Vaccine Initiative](http://satvi.uct.ac.za/)

# 3 Protein Pair Ratio  

#### About

A simple R-script for computing the 3PR score for a plasma/serum sample with a protein profile generated in a SOMAscan Assay. The 3PR model is a self-normalizing (i.e. obviating the need for reference-based standardisation) protein-based risk signature for Tuberculosis disease progression developed by applying the Pair Ratios algorithm to the South African Adolescent Cohort Study (ACS) progressors (cases).

#### Structure of 3PR
![](figures/3pr.png)

#### How TO:

- source('3pr.R')
- proteinPairRatioScore <- pr3(somaData)


Email: Stanley.Kimbung@uct.ac.za